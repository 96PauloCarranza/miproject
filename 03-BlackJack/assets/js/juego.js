//*BlackJack*/


let deck = [];
const tipos = ['C','D','H','S'];
const especiales = ['A','J','Q','K'];

let puntosJugador = 0,
    puntosComputadora = 0;
//Referencias del HTML
const btnPedir = document.querySelector('#btnPedir');
//console.log(btnPedir);
const divCartasJugador      = document.querySelector('#jugador-cartas');
const divCartasComputadora  = document.querySelector('#computadora-cartas');
const puntosHTML            = document.querySelectorAll('small');

const btnDetener = document.querySelector('#btnDetener');
const btnNuevo = document.querySelector('#btnNuevo');

//Esta funcion te permite crear la baraja de cartas
const creaDeck = () => {

    for(let i = 2; i <=10 ; i++){
       // deck.push(i+'C');
       for(let tipo of tipos){
           deck.push(i+ tipo);
       }
    }

    for(let tipo of tipos){
        for(let esp of especiales){
            deck.push(esp + tipo);
        }
    }
    deck = _.shuffle(deck);

    console.log(deck);

    return deck;
    
}
creaDeck();  

const pedirCartas = () => {
    if(deck.length === 0){
        throw 'No hay cartas en la baraja'
    }
    const carta = deck.pop();
   // console.log(carta);
    //console.log(deck);

    return carta;

}
/*for(let i = 0; i<=60 ; i++){
pedirCartas();
}*/

const valorCarta = (carta) => {

    const valor = carta.substring(0, carta.length - 1);
   // let puntos = 0;

    return (isNaN (valor)) ? 
            (valor === 'A') ? 11 : 10
            : valor * 1;


}

//Turno de la computadora

const turnoComputadora = (puntosMinimos) => {
    do{
    const carta = pedirCartas();
    puntosComputadora = puntosComputadora + valorCarta(carta);
    puntosHTML[1].innerText = puntosComputadora;

    //<img class="carta" src="assets/cartas/10C.png">
    const imgCarta = document.createElement('img'); 
    imgCarta.src  = `assets/cartas/${carta}.png`; // crear cartas dinamicas
    imgCarta.classList.add('carta');
    divCartasComputadora.append(imgCarta);
    
    if( puntosMinimos > 21){
        break;
    } 

    }while( (puntosComputadora < puntosMinimos) && (puntosMinimos <= 21));

    setTimeout(() => {
        if(puntosComputadora === puntosMinimos){
            alert('Nadie gana');
        } else if(puntosMinimos >21){
            alert('Computadora gana');
        }else if(puntosComputadora > 21){
            alert('Jugador gana');
        }else if (puntosJugador ===21){
            alert('Jugador gana');
        }else{
            alert('computadora gana');
        }
    },100 );
}


//valorCarta('3D');


//Evento

btnPedir.addEventListener('click',() =>{

    const carta = pedirCartas();
    puntosJugador = puntosJugador + valorCarta(carta);
    puntosHTML[0].innerText = puntosJugador;

    //<img class="carta" src="assets/cartas/10C.png">
    const imgCarta = document.createElement('img');

    imgCarta.src  = `assets/cartas/${carta}.png`; // crear cartas dinamicas
    imgCarta.classList.add('carta');
    divCartasJugador.append(imgCarta);


    if(puntosJugador > 21 ){
        console.warn('Lo siento mucho, perdiste');
        btnPedir.disabled = true;
        btnDetener.disabled = true;
        turnoComputadora(puntosJugador);
    }else if (puntosJugador === 21){
        console.warn('21, GANASTE');
        btnPedir.disabled = true;
        btnDetener.disabled = true;
        turnoComputadora();
    }
});


btnDetener.addEventListener('click', () => {
    btnPedir.disabled = true;
    btnDetener.disabled = true;

    turnoComputadora(puntosJugador);
});

btnNuevo.addEventListener('click', () =>{
    console.clear();
    deck =[];
    deck = creaDeck();
    puntosJugador = 0,
    puntosComputadora = 0;
    puntosHTML[0].innerText = 0;
    puntosHTML[1].innerText = 0;

    divCartasComputadora.innerText = '';
    divCartasJugador.innerText = '';

    btnPedir.disabled = false;
    btnDetener.disabled = false;

});
//TODO: Borrar
/*console.log(15);
turnoComputadora(15);
*/
